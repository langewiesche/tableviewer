package dao;

import java.sql.*;

/**
 * Verbindungsklasse zur Datenbank. Jedes Db-Objekt verwaltet genaue eine
 * Db-Verbindung. Kann auch mit try-with-resources verwendet werden.
 *
 * @author rla
 */
public class Db implements java.lang.AutoCloseable {

    public String driver = "org.hsqldb.jdbcDriver";
    public String url = "jdbc:hsqldb:hsql://localhost";
    public String usr = "SA";
    public String passwd = "";

    private Connection con = null;

    /**
     * Baut eine Datenbankverbindung auf, wenn erforderlich
     *
     * @return
     */
    public Connection getCon() {
        // Treiber laden wenn erforderlich
        try { // kann unter JDK 8 entfallen
            Class.forName(driver);
            // Aufbau der Verbindung, wenn erforderlich
            if (con == null) { // Verbindung wurde noch nicht aufgebaut
                con = DriverManager.getConnection(
                        url, usr, passwd);
            } else if (!con.isValid(0)) { // Verbindung ist nicht mehr gueltig
                con = DriverManager.getConnection(
                        url, usr, passwd);
            }
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println("Fehler: " + e + " beim Verbindungsaufbau.");
        }
        return con;
    }

    /**
     * Schliesst die Datenbankverbindung, falls moeglich.
     *
     */
    @Override
    public void close() {
        if (con != null) {
            try {
                con.close();
            } catch (SQLException e) {
                System.err.println("Fehler: " + e + " beim Schliessen der Verbindung.");
            } finally {
                con = null;
            }
        }
    }

}
